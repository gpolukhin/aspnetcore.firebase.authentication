﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;

namespace AspNetCore.Firebase.Authentication.Extensions
{
	public static class ApplicationBuilderExtensions
	{
		public static IApplicationBuilder UseFirebaseAuthentication(this IApplicationBuilder app, string issuer, string audience)
		{
			app.UseJwtBearerAuthentication(new FirebaseBearerOptions(issuer, audience));
			return app;
		}

		public static IApplicationBuilder UseFirebaseAuthentication(this IApplicationBuilder app, string firebaseProject)
		{
			app.UseJwtBearerAuthentication(new FirebaseBearerOptions("https://securetoken.google.com/" + firebaseProject, firebaseProject));

			return app;
		}
	}
}
