﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Builder;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography.X509Certificates;
using System.Security.Claims;
using System.Text;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;

namespace AspNetCore.Firebase.Authentication
{
	public class FirebaseBearerOptions : JwtBearerOptions
	{
		/// <summary>
		/// The Jwt Middleware will retrieve signing certificates using issuer endpoint and OpenIdConnection
		/// e.g https://securetoken.google.com/MY_PROJECT/.well-known/openid-configuration
		/// </summary>
		/// <param name="validIssuer"></param>
		/// <param name="validAudience"></param>
		public FirebaseBearerOptions(string validIssuer, string validAudience)
		{
			AutomaticAuthenticate = true;
			AutomaticChallenge = true;
			AuthenticationScheme = JwtBearerDefaults.AuthenticationScheme;
			IncludeErrorDetails = true;
			Authority = validIssuer;
			Audience = validAudience;

			TokenValidationParameters = new TokenValidationParameters
			{
				RequireExpirationTime = true,
				RequireSignedTokens = true,
				SaveSigninToken = false,
				ValidateActor = false,
				ValidateAudience = true,
				ValidAudience = validAudience,
				ValidateIssuer = true,
				ValidIssuer = validIssuer,
				ValidateIssuerSigningKey = true,
				ValidateLifetime = true,

				ClockSkew = TimeSpan.Zero
			};
		}
	}
}
