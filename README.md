# README #

![Build status](https://raphaelbickel.visualstudio.com/_apis/public/build/definitions/dfcb2c0d-0dee-45e6-b6b2-d95daac86af9/1/badge)

### What is this repository for? ###

* JwtBearer Firebase authentication middleware for AspNetCore 1.1 (and higher)

### Setup a Firebase project ###

* Create a Firebase project. https://console.firebase.google.com/

**Issuer** will be *https://securetoken.google.com/MYPROJECTNAME* and **Audience** will be *MYPROJECTNAME*

Enable authentication provider (Google, Facebook, Anonymous, OTP, ...) on your project.

### Use AspNetCore.Firebase.Authentication in AspNetCore <= 1.1 web application ###

```
		 Install-Package AspNetCore.Firebase.Authentication
```
https://www.nuget.org/packages/AspNetCore.Firebase.Authentication/


* In Startup.cs -> 

```csharp
		public void ConfigureServices(IServiceCollection services)
		{
			services.AddAuthentication();
		}
```


```csharp
		public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
		{
			app.UseFirebaseAuthentication("https://securetoken.google.com/MYPROJECTNAME", "MYPROJECTNAME");
			//Or since version 1.0.6
			app.UseFirebaseAuthentication("MYPROJECTNAME");
		}
```

* Just have to use [Authorize] attribute on your controllers to enforce authorization